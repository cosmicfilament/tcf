import { createGlobalStyle } from 'styled-components/macro';
import { setColor, setFont } from '../../styles';
const GlobalStyles = createGlobalStyle`

* {
    margin: 0;
    padding: 0;
}

*,
*::after,
*::before {
	box-sizing: inherit;
}

html {
  box-sizing: border-box;
  font-size: 62.5%; 
}

body{
  color:${setColor.primaryText};
  font-weight: 400;
  line-height: 1.6;
  
  background: linear-gradient(to bottom, ${setColor.bkgnd_light} 0%, ${setColor.bkgnd_dark} 100%); 
  
  background-size: cover;
  background-repeat: no-repeat;
  min-height: 100vh;
  font-family: ${setFont.primary};
}

section, article, aside, footer, p {
  font-size: 1.75rem;
  margin: 0.67rem 0;
}

header {
  font-size: 4.5rem;
  margin: 1rem 0;
}
h1 {
  font-size: 4rem;
  margin: 0.67rem 0;
}

h2 {
  font-size: 3rem;
  margin: 0.67rem 0;
}

h3 {
  font-size: 2.5rem;
  margin: 0.67rem 0;
}

`;

export default GlobalStyles;
