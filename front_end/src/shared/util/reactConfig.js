export const LOG_IT = false;

const BASE_URL_DEVELOPMENT = 'http://localhost:5000';
const BASE_URL_PRODUCTION = 'http://twitterdumpsterfire.com:80';

export const BASE_URL = function () {
	return process.env.NODE_ENV === 'development'
		? BASE_URL_DEVELOPMENT
		: BASE_URL_PRODUCTION;
};

export const GET_BLOGLIST = '/api/blogs';
