import React, { useEffect, useState, Fragment } from 'react';
// import uuid from 'react-uuid';
// import styled from 'styled-components/macro';
import ErrorModal from '../shared/components/UIElements/ErrorModal';
import { useHttpClient } from '../shared/hooks/httpHook';
import { GET_BLOGLIST } from '../shared/util/reactConfig';

// import { setFont, setColor } from '../styles';
// import BlogPost from '../blogPost/BlogPost';

const BlogList = props => {
	// encapsulates the fetch request
	const { isLoading, error, sendRequest, clearError } = useHttpClient();
	const [ list, setList ] = useState([]);

	useEffect(
		() => {
			async function init () {
				try {
					const params = {};
					const responseData = await sendRequest(GET_BLOGLIST, params);
					const blogList = responseData.blogList;
					console.log(JSON.stringify(blogList));
					setList(blogList);
				} catch (error) {
					console.log(JSON.stringify(error));
				}
			}
			init();
		},
		[ sendRequest ]
	);

	return (
		<Fragment>
			<ul>
				<li>one list item</li>
			</ul>
			<ErrorModal error={error} onClear={clearError} />
		</Fragment>
	);
};

export default BlogList;
