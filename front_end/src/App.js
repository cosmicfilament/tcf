import React from 'react';
import styled from 'styled-components/macro';

import GlobalStyles from './shared/styles/GlobalStyles';
import { setColor, centerText } from './styles';
import Header from './header/Header';
import Main from './main/Main';
import Footer from './footer/Footer';

function App () {
	return (
		<React.Fragment>
			<StyledApp>
				<GlobalStyles />
				<Header />
				<Main />
				<Footer />
			</StyledApp>
		</React.Fragment>
	);
}

export default App;

const StyledApp = styled.div`
	${centerText};
	background-color: ${setColor.bkgndMain};
`;
