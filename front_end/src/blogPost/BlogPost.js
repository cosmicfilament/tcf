import React from 'react';
import styled from 'styled-components/macro';

import { setFont, setColor } from '../styles';

const BlogPost = props => {
	return <StyledTitle>{props.title} </StyledTitle>;
};

export default BlogPost;

const StyledTitle = styled.h2``;
