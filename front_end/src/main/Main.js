import React, { Fragment } from 'react';
//import styled from 'styled-components/macro';

//import { setFont, setColor } from '../styles';
import Me from '../me/Me';
import BlogList from '../bloglist/BlogList';

const Main = () => {
	return (
		<Fragment>
			<Me />
			<BlogList />
		</Fragment>
	);
};

export default Main;
