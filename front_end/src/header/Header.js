import React from 'react';
import styled from 'styled-components/macro';
import { setFont, setColor } from '../styles';

const Header = () => {
	return <StyledHeader>The Cosmic Filament.</StyledHeader>;
};

export default Header;

const StyledHeader = styled.header`
	font-family: ${setFont.heading};
	color: ${setColor.complColor3};
	-webkit-text-stroke: 1px ${setColor.primaryText};
	cursor: pointer;
`;
