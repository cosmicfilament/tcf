'use strict';

/**
    * @module apiRouter
    * @author John Butler
    * @module default express router file
*/

const express = require('express');
const router = express.Router();

const blogList = require('../models/blogList').instance;
const logs = require('../util/logs');

// router.get('/get', (req, res) => router.getSomething(req, res));

// router.post('/post', (req, res) => router.postSomething(req, res));

// router.getSomething = async (req, res) => {
// 	const query = req.query;
// 	logs.log(`getSomething query: ${JSON.stringify(query)}`, 'green');
// 	res.json({ msg: 'Get Something', query });
// };

// router.postSomething = async (req, res) => {
// 	const body = req.body;
// 	logs.log(`postSomething body: ${JSON.stringify(body)}`, 'green');
// 	res.json({ msg: 'Post Something', body });
// };

router.get('/blogs', (req, res) => router.getBlogs(req, res));
router.get('/blogs/:name', (req, res) => router.getBlog(req, res));

router.getBlogs = async (req, res) => {
	logs.log(`retrieving blogs`);
	res.json(blogList.list);
};

router.getBlog = async (req, res) => {
	const name = req.query.name;
	logs.log(`retrieving blog: ${name}`);
	res.json(blogList.find(name));
};

module.exports = router;
