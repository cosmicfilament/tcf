'use strict';

/**
  * @module blogRouter.js
  * @author John Butler
  * @description routes blog requests
*/

const express = require('express');
const router = express.Router();

//const blogList = require('../models/blogList').instance;
//const logs = require('../util/logs');

//router.get('/', (req, res) => router.getBlogs(req, res));
//router.get('/:name', (req, res) => router.getBlog(req, res));

// router.getBlogs = async (req, res) => {
// 	logs.log(`retrieving blogs`);
// 	res.json(blogList);
// };

// router.getBlog = async (req, res) => {
// 	const name = req.query.name;
// 	logs.log(`retrieving blog: ${name}`);
// 	res.json(blogList.find(name));
// };
