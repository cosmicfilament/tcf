'use strict';

/**
  * @module blogPost.js
  * @author John Butler
  * @description  
*/

class BlogPost {
	constructor (name = null) {
		this._name = name;
		this._body = null;
	}

	get name () {
		return this._name;
	}
	set name (name) {
		this._name = name;
		return this;
	}

	get body () {
		return this._body;
	}
	set body (body) {
		this._body = body;
		return this;
	}
}

module.exports = BlogPost;
