'use strict';

/**
  * @module blogList.js
  * @author John Butler
  * @description 
*/

// BlogList is an array of BlogPosts which each have:
// filename
// title
// date
// summary
// body

// https://medium.com/@dmnsgn/singleton-pattern-in-es6-d2d021d150ae
const singleton = Symbol();
const singletonEnforcer = Symbol();

class BlogList {
	constructor (enforcer) {
		this._list = [];
		this._type = 'singleton';

		if (enforcer !== singletonEnforcer) {
			throw new Error(
				'Cannot construct a new BlogList. use the get instance static method.'
			);
		}
	}

	static get instance () {
		if (!this[singleton]) {
			this[singleton] = new BlogList(singletonEnforcer);
		}
		return this[singleton];
	}

	get type () {
		return this._type;
	}

	get list () {
		return this._list;
	}

	set list (list) {
		this._list = [ ...list ];
		return this;
	}

	push (item) {
		this._list.push(item);
		return this;
	}

	pop () {
		this._list.pop();
		return this;
	}

	add (item) {
		return this.push(item);
	}

	remove (name) {
		this._list = this._list.filter(item => item.name !== name);
		return this;
	}

	find (name) {
		return this._list.find(item => item.name === name);
	}
}

module.exports = BlogList;
