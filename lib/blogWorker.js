'use strict';

/**
  * @module blogWorker.js
  * @author John Butler
  * @description loads the bloglist and parses it into a json object
*/

const logs = require('../util/logs');
const files = require('../util/fileP');
const nodeConfig = require('../util/nodeConfig');
const BlogList = require('../models/blogList');
const BlogPost = require('../models/blogPost');

const intervalTime = nodeConfig.BLOG_LIST_CHECK;
const logDir = `${nodeConfig.BASE_DIR}/${nodeConfig.BLOG_DIR}`;
const makeFile = file => `${logDir}/${file}`;

async function checkLoadBlogList () {
	const blogFileNames = await files.readDirectory(logDir);
	if (!blogFileNames) {
		return false;
	}

	let lastTime = new Date(Date.now());
	// subtract the amount of time between checks
	lastTime = lastTime.setTime(lastTime.getTime() - intervalTime);
	// get the singleton list of blogs
	const theList = BlogList.instance;

	for (let name of blogFileNames) {
		const file = makeFile(name);
		const listItem = theList.find(name);
		const post = await files.readFileAsUtf8(file);
		// new blogPost
		if (!listItem) {
			let bp = new BlogPost();
			bp.name = name;
			bp.body = post;
		}
		else {
			const stat = await files.stats(file);
			let fileTime = new Date(stat.mtime);
			fileTime = fileTime.getTime();
			// blogPost updated
			if (fileTime >= lastTime) {
				listItem.body = post;
			}
		}
	}
	return true;
}

function blogListLoop () {
	setInterval(() => {
		if (!checkLoadBlogList()) {
			logs.log('Error loading Blog List during checkLoadBlogList');
		}
	}, intervalTime);
}

function initBlogList () {
	logs.log('Blog List loader background worker has started.', 'blue');
	blogListLoop();
	return checkLoadBlogList();
}

module.exports = initBlogList;
