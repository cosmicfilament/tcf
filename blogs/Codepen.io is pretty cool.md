# Hey! Codepen is pretty cool

_February 23, 2020_ 

**Brief tutorial on setting up a template in codepen**

I would like to start out by saying that I have absolutely no affiliation with codepen. I actually started using it only within the last month and have found that I really like it for small things that I just don't want to use vscode for. For instance recently I was reading this article in medium [Understanding Promises in Javascript](https://medium.com/better-programming/understanding-promises-in-javascript-13d99df067c1) and I wanted to run the code for 3 of the examples that were in the article. I had heard of [Codepen](https://codepen.io/),  [jsfiddle](https://jsfiddle.net/) and [CodeSandbox](https://codesandbox.io/), so I played around with all 3 and ended up settling on  using Codepen for the examples. I really like the way that Codepen lays out the html, css and javascript editors side by side and the level of complexity is just right for what I want to use it for. I especially didn't need a full blown editor such as CodeSandbox which kept crashing on me anyway.  Jsfiddle is probably a good alternative, too.

Anyway let's get down to what this post is actually about. So, this is my first blog post and I thought that I would start out with something simple that I just figured out, which is how to create a template in codepen. A template allows you to setup once and then not have to recreate over and over the boilerplate stuff that is required for each and every pen.
